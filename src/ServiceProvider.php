<?php

namespace Dottystyle\LaravelArchiver;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {   
        $root = dirname(__DIR__);

        $this->publishes([
            "$root/config.php" => config_path('archive.php')
        ]);

        $this->mergeConfigFrom("$root/config.php", 'archive');
        
        $this->loadMigrationsFrom("$root/migrations");
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('archive', function ($app) {
            return new Manager($app);
        });
    }

    /**
     * Get list of provided services.
     * 
     * @return 
     */
    public function provides()
    {
        return ['archive'];
    }
}