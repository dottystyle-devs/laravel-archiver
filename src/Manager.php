<?php

namespace Dottystyle\LaravelArchiver;

use Illuminate\Support\Manager as BaseManager;

class Manager extends BaseManager
{
    /**
     * Get the default driver.
     * 
     * @return string
     */
    public function getDefaultDriver()
    {
        return 'eloquent';
    }

    /**
     * Create an eloquent archive manager.
     * 
     * @return \Dottystyle\LaravelArchiver\ArchiveManager
     */
    protected function createEloquentDriver()
    {
        return new Eloquent\EloquentArchiveManager($this->createDbArchiveProvider('eloquent'));
    }

    /**
     * Create archive provider using the database.
     * 
     * @param string $type
     * @return \Dottystyle\LaravelArchiver\DatabaseArchiveProvider
     */
    protected function createDbArchiveProvider($type)
    {
        return new DatabaseArchiveProvider($type, $this->app['db']->connection(), 'archives');
    }
}