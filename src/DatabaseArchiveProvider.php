<?php

namespace Dottystyle\LaravelArchiver;

use Dottystyle\LaravelArchiverr\Contracts\Archive;
use Illuminate\Database\ConnectionInterface;

class DatabaseArchiveProvider
{
    /**
     * @var string
     */
    protected $type;

    /**
     * @var \Illuminate\Contracts\ConnectionInterface
     */
    protected $db;

    /**
     * @var string
     */
    protected $table;

    /**
     * Craete new instance of archive provider.
     * 
     * @param string $type
     * @param \Illuminate\Contracts\ConnectionInterface $db
     * @param string $table 
     */
    public function __construct($type, ConnectionInterface $db, $table)
    {
        $this->type = $type;
        $this->db = $db;
        $this->table = $table;
    }

    /**
     * Save the given archive and return its id.
     * 
     * @param string $subtype
     * @param string $name
     * @param mixed $contents
     * @param string|int $referenceId (optional)
     * @return string|int
     */
    public function save($subtype, $name, $contents, $referenceId = null)
    {
        $data = [
            'type' => $this->type,
            'subtype' => $subtype,
            'name' => $name,
            'contents' => serialize($contents),
            'created_at' => now()->toDateTimeString()
        ];

        if (isset($referenceId)) {
            $data['ref_id'] = $referenceId;
        }

        return $this->query(false)->insertGetId($data);
    }

    /**
     * Get the archive with the given id.
     * 
     * @param string|int $id
     * @return object|null
     */
    public function get($id)
    {
        if ($data = $this->whereId($id)->first()) {
            $data->contents = unserialize($data->contents);
        }

        return $data;
    }

    /**
     * Determine whether there is an existing archive with the given id.
     * 
     * @param string|int $id
     * @return bool
     */
    public function has($id)
    {
        return $this->whereId($id)->exists();
    }

    /**
     * Create query with id constraint applied.
     * 
     * @param string|int $id
     * @return \Illuminate\Database\Query\Builder
     */
    protected function whereId($id)
    {
        return $this->query()->where('id', $id);
    }

    /**
     * Delete archive by its id.
     * 
     * @param string|int $id
     * @return bool
     */
    protected function delete($id)
    {
        return $this->whereId($id)->delete();
    }

    /**
     * Begin a new database query from the source table.
     * 
     * @param bool $typeConstraint (optional)
     * @return \Illuminate\Database\Query\Builder
     */
    protected function query($typeConstraint = true)
    {
        $query = $this->db->table($this->table);
        
        return $typeConstraint ? $query->where('type', $this->type) : $query;
    }
}