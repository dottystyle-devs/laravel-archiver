<?php

namespace Dottystyle\LaravelArchiver;

use Dottystyle\LaravelArchiver\Contracts\Archive;
use Dottystyle\LaravelArchiver\Contracts\Archiver;
use Dottystyle\LaravelArchiver\Contracts\Store;

class ArchiveManager
{
    /**
     * @var \Dottystyle\LaravelArchiver\Contracts\Archiver
     */
    protected $archiver;

    /**
     * @var \Dottystyle\LaravelArchiver\Contracts\Store
     */
    protected $store;

    /**
     * Create new instance of an archive manager.
     * 
     * @param \Dottystyle\LaravelArchiver\Contracts\Archiver $archiver
     * @param \Dottystyle\LaravelArchiver\Contracts\Store $store
     */
    public function __construct(Archiver $archiver, Store $store)
    {
        $this->archiver = $archiver;
        $this->store = $store;
    }

    /**
     * Get the archiver used.
     * 
     * @return \Dottystyle\LaravelArchiver\Contracts\Archiver
     */
    public function getArchiver()
    {
        return $this->archiver;
    }

    /**
     * Get the store instance for archives.
     * 
     * @return \Dottystyle\LaravelArchiver\Contracts\Store
     */
    public function getStore()
    {
        return $this->store;
    }

    /**
     * Create an archive of the archivable object.
     * 
     * @param mixed $subject
     * @param string $name
     * @return string|int
     */
    public function archive($subject, $name)
    {
        return $this->archiver->archive($subject, $name);
    }

    /**
     * Restore the given archive.
     * 
     * @param \Dottystyle\LaravelArchiver\Contracts\Archive
     * @return mixed
     */
    public function restore(Archive $archive)
    {
        return $this->archiver->restore($archive);
    }

    /**
     * Save the given archive and return its id.
     * 
     * @param \Dottystyle\LaravelArchiver\Contracts\Archive $archive
     * @return string|int
     */
    public function save(Archive $archive)
    {
        return $this->store->save($archive);
    }

    /**
     * Get the archive with the given id.
     * 
     * @param string|int $id
     * @return \Dottystyle\LaravelArchiver\Contracts\Archive
     */
    public function get($id)
    {
        return $this->store->get($id);
    }

    /**
     * Determine whether there is an existing archive with the given id.
     * 
     * @param string|int $id
     * @return bool
     */
    public function has($id)
    {
        return $this->store->has($id);
    }
}