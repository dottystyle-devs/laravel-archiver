<?php

namespace Dottystyle\LaravelArchiver\Contracts;

interface Store
{
    /**
     * Save the given archive and return its id.
     * 
     * @param \Dottystyle\LaravelArchiver\Contracts\Archive $archive
     * @return string|int
     */
    public function save(Archive $archive);

    /**
     * Get the archive with the given id.
     * 
     * @param string|int $id
     * @return \Dottystyle\LaravelArchiver\Contracts\Archive
     */
    public function get($id);

    /**
     * Determine whether there is an existing archive with the given id.
     * 
     * @param string|int $id
     * @return bool
     */
    public function has($id);
}