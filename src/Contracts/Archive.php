<?php

namespace Dottystyle\LaravelArchiver\Contracts;

use Carbon\Carbon;

interface Archive 
{
    /**
     * Get name of the archive.
     * 
     * @return string
     */
    public function getName();

    /**
     * Get the date the archive was created.
     * 
     * @return \Carbon\Carbon
     */
    public function getCreatedAt() : Carbon;

    /**
     * Get the size of archive.
     * 
     * @return int
     */
    public function getSize();

    /**
     * Get the contents of the archive.
     * 
     * @return mixed
     */
    public function getContents();
}