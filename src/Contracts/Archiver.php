<?php

namespace Dottystyle\LaravelArchiver\Contracts;

interface Archiver
{
    /**
     * Create an archive of the archivable object.
     * 
     * @param mixed $subject
     * @param string $name
     * @return \Dottystyle\LaravelArchiver\Contracts\Archive
     */
    public function archive($subject, $name) : Archive;

    /**
     * Restore the given archive.
     * 
     * @param \Dottystyle\LaravelArchiver\Contracts\Archive
     * @return mixed
     */
    public function restore(Archive $archive);
}