<?php

namespace Dottystyle\LaravelArchiver\Eloquent;

use Dottystyle\LaravelArchiver\Contracts\Archive;
use Dottystyle\LaravelArchiver\Contracts\Store;
use Dottystyle\LaravelArchiver\DatabaseArchiveProvider;

class EloquentStore implements Store
{
    /**
     * @var \Dottylstyle\LaravelArchiver\DatabaseArchiveProvider 
     */
    protected $archives;

    /**
     * @var array
     */
    protected $modelMapping = [];

    /**
     * Create new instance of the store.
     * 
     * @param \Dottylstyle\LaravelArchiver\DatabaseArchiveProvider $provider
     */
    public function __construct(DatabaseArchiveProvider $provider)
    {
        $this->archives = $provider;
    }
    
    /**
     * Save the given archive and return its id.
     * 
     * @param \Dottystyle\LaravelArchiver\Contracts\Archive $archive
     * @return string|int
     */
    public function save(Archive $archive)
    {
        $modelClass = get_class($model = $archive->getModel());
        $subType = ($index = array_search($modelClass, $this->modelMapping)) !== false ? $index : $modelClass;

        return $this->archives->save(
            $subType ?: $modelClass,
            $archive->getName(),
            $archive->getContents(), 
            $model->getKey() ?? null
        );
    }

    /**
     * Get the archive with the given id.
     * 
     * @param string|int $id
     * @return \Dottystyle\LaravelArchiver\Contracts\Archive
     */
    public function get($id)
    {
        $data = $this->archives->get($id);

        if (empty($data)) {
            return null;
        }

        return new EloquentArchive($data->contents, $data->name, $data->ref_id);
    }

    /**
     * Determine whether there is an existing archive with the given id.
     * 
     * @param string|int $id
     * @return bool
     */
    public function has($id)
    {
        return $this->archives->has($id);
    }
    
    /**
     * Map custom model names.
     * 
     * @param array $map
     * @return void
     */
    public function modelMap(array $map)
    {
        $this->modelMapping = array_merge($this->modelMapping, $map);
    }
}