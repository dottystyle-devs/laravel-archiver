<?php

namespace Dottystyle\LaravelArchiver\Eloquent;

use Dottystyle\LaravelArchiver\ArchiveManager;
use Dottystyle\LaravelArchiver\DatabaseArchiveProvider;

class EloquentArchiveManager extends ArchiveManager
{
    /**
     * Create new instance of the eloquent archive manager.
     * 
     * @param \Dottystyle\LaravelArchiver\DatabaseArchiveProvider $provider
     */
    public function __construct(DatabaseArchiveProvider $provider)
    {
        parent::__construct(new EloquentArchiver(), new EloquentStore($provider));
    }

    /**
     * Map custom model names.
     * 
     * @param array $map
     * @return mixed
     */
    public function modelMap(array $map)
    {
        return $this->store->modelMap($map);
    }
}