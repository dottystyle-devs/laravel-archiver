<?php

namespace Dottystyle\LaravelArchiver\Eloquent;

interface Archivable
{
    /**
     * A related data/object has been archived.
     * 
     * @param string $name
     * @param mixed $related
     * @return void
     */
    public function relatedArchived($name, $related);

    /**
     * Get the default name for the archive.
     * 
     * @return string.
     */
    public function getDefaultArchiveName();

    /**
     * Get the attributes to be archived.
     * 
     * @return array
     */
    public function getArchivableAttributes();

    /**
     * Get an array of key/value pairs of related things to archive.
     * 
     * @return array
     */
    public function getRelatedArchivables();

    /**
     * The instance is to be archived as related model/instance of the given parent model.
     * 
     * @param \Illuminate\Database\Eloquent\Model $parent
     * @return void
     */
    public function archiveAsRelated($parent);
}