<?php

namespace Dottystyle\LaravelArchiver\Eloquent;

use Dottystyle\LaravelArchiver\Contracts\Archive;
use Dottystyle\LaravelArchiver\Contracts\Archiver;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Builder;
use Closure;

class EloquentArchiver implements Archiver
{
    /**
     * Create an archive of the archivable object.
     * 
     * @param \Illuminate\Database\Eloquent\Model $model
     * @param string $name
     * @return \Dottystyle\LaravelArchiver\Contracts\Archive
     */
    public function archive($model, $name) : Archive
    {
        return (new EloquentArchive(
            $this->archiveModel($model), $name
        ))->setModel($model);
    }

    /**
     * Get the archive data for the given model.
     * 
     * @param \Illuminate\Database\Eloquent\Model $model
     * @param bool $related
     * @return array
     */
    protected function archiveModel(Model $model)
    {
        if ($model instanceof Archivable) {
            $data = array_merge([
                'attributes' => $model->getArchivableAttributes()
            ], $this->archiveRelated($model));
        } else {
            $data = ['attributes' => $model->toArray()];
        }

        return $data;
    }

    /**
     * Archive others related to the the model.
     * 
     * @param \Illuminate\Database\Eloquent\Model $model
     * @return array
     */
    protected function archiveRelated(Model $model)
    {
        if (! ($archivables = $model->getRelatedArchivables())) {
            return [];
        }

        // Before archiving the related objects/data, we will eager load first the necessary relationships
        $this->eagerLoadRelations($model, $archivables);
        
        $data = [];
        // Archive the related objects/data.
        // A related object/data can be one of the following:
        //   string - Relation name
        //   \Illuminate\Database\Eloquent\Collection
        //   \Illuminate\Database\Eloquent\Builder
        //   \Illumintae\Database\Eloquent\Model
        //   \Closure
        foreach ($archivables as $name => $value) {
            // Attempt to load the missing relations
            if (is_string($value)) {
                if ($related = $model->getRelationValue($value)) {
                    $data[$value] = $this->archiveRelatedRelation($model, $value, $related);
                }
            } else if ($value instanceof Collection) {
                $data[$name] = $this->archiveRelatedCollection($model, $name, $value);
            } else if ($value instanceof Builder) {
                $data[$name] = $this->archiveRelatedCollection($model, $name, $value->get());
            } else if ($value instanceof Model) {
                $data[$name] = $this->archiveRelatedModel($model, $name, $value);
            } else if (! is_null($result = value($value))) {
                $model->relatedArchived($name, $data[$name] = $result);
            }
        }
        
        return $data;
    }

    /**
     * Eager load relationships on the model.
     * 
     * @param \Illuminate\Database\Eloquent\Model $model
     * @param array $archivables
     * @return void
     */
    protected function eagerLoadRelations(Model $model, array $archivables)
    {
        if ($relations = array_filter($archivables, 'is_string')) {
            $model->loadMissing($relations);
        }
    }

    /**
     * Archive a related model collection.
     * 
     * @param \Illuminate\Database\Eloquent\Model $model
     * @param string $name
     * @param mixed $relation
     * @return array
     */
    protected function archiveRelatedRelation(Model $model, $name, $relation)
    {
        if ($relation instanceof Collection) {
            return $this->archiveRelatedCollection($model, $name, $relation);
        } else {
            return $this->archiveRelatedModel($model, $name, $relation);
        }
    }

    /**
     * Archive a related model collection.
     * 
     * @param \Illuminate\Database\Eloquent\Model $model
     * @param \Illuminate\Database\Eloquent\Collection $collection
     * @return array
     */
    protected function archiveRelatedCollection(Model $model, $name, Collection $collection)
    {
        return $collection->map(function ($related) use ($model, $name) {
            return $this->archiveRelatedModel($model, $name, $related);
        })->all();
    }

    /**
     * Archive the related model.
     * 
     * @param \Dottystyle\LaravelArchiver\Eloquent\Archivable $archivable
     * @param string $name
     * @param \Illuminate\Database\Eloquent\Model $related
     * @return array
     */
    protected function archiveRelatedModel(Model $model, $name, Model $related)
    {
        $data = $this->archiveModel($related);
        $model->relatedArchived($name, $related);

        return $data;
    }

    /**
     * Restore the given archive.
     * 
     * @param \Dottystyle\LaravelArchiver\Archive
     * @return \Dottystyle\LaravelArchiver\Contracts\Archive
     */
    public function restore(Archive $archive)
    {
    }
}