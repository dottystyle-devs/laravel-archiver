<?php

namespace Dottystyle\LaravelArchiver\Eloquent;

use Dottystyle\LaravelArchiver\Contracts\Archive;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class EloquentArchive implements Archive
{
    /**
     * @var \Illuminate\Database\Eloquent\Model
     */
    protected $model;

    /**
     * @var string
     */
    protected $name = '';

    /**
     * @var array
     */
    protected $data;

    /**
     * @var \Carbon\Carbon
     */
    protected $createdAt;

    /**
     * Create new eloquent archive.
     * 
     * @param array $data
     * @param string $name
     * @param \Carbon\Carbon|string $createdAt
     */
    public function __construct(array $data, $name, $createdAt = null)
    {
        $this->data = $data;
        $this->name = $name;
        $this->createdAt = $createdAt ?? Carbon::now();
    }

    /**
     * Get name of the archive.
     * 
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get the date the archive was created.
     * 
     * @return \Carbon\Carbon
     */
    public function getCreatedAt() : Carbon
    {
        return $this->createdAt;
    }

    /**
     * Get the size of archive.
     * 
     * @return int
     */
    public function getSize()
    {
        return strlen($this->data);
    }

    /**
     * Get the data of the archive.
     * 
     * @return mixed
     */
    public function getContents()
    {
        return $this->data;
    }

    /**
     * Get the source model of the archive.
     * 
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function getModel() : Model
    {
        return $this->model;
    }

    /**
     * Set the source model of the archive.
     * 
     * @return static
     */
    public function setModel(Model $model)
    {
        $this->model = $model;

        return $this;
    }
}