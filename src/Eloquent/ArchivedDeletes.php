<?php

namespace Dottystyle\LaravelArchiver\Eloquent;

use Illuminate\Database\Eloquent\Model;

trait ArchivedDeletes
{
    /**
     * @var \Dottystyle\LaravelArchiver\Eloquent\EloquentArchive
     */
    protected $preparedArchive;

    /**
     * @var bool
     */
    protected $archivingAsRelated = false;

    /**
     * Boot the archived deleting trait.
     * 
     * @return void 
     */
    public static function bootArchivedDeletes()
    {
        // We will prepare the archive of the model before deletion so
        // we can still save the relationships to include on the archive.
        static::deleting(function ($model) {
            if ($model->shouldArchiveOnDelete()) {
                $model->preparedArchive = $model->prepareArchive();
            }
        });

        // Save the prepared archived created during the "deleting" callback.
        static::deleted(function ($model) {
            if (isset($model->preparedArchive)) {
                $model->saveArchive($model->preparedArchive);
            }
        });
    }

    /**
     * Archive the raw attributes by default.
     * 
     * @return array
     */
    public function getArchivableAttributes()
    {
        return $this->attributes;
    }

    /**
     * Delete and archive the model.
     * 
     * @return \Dottystyle\LaravelArchiver\Eloquent\EloquentArchive|false
     */
    public function archive()
    {
        if ($this->delete() && isset($this->preparedArchive)) {
            return $this->preparedArchive;
        }
    }

    /**
     * A related object/data has been archived.
     * 
     * @param string $name
     * @param mixed $related
     * @return void
     */
    public function relatedArchived($name, $related)
    {
        // Delete archived related models
        if ($related instanceof Model) {
            if ($related instanceof Archivable) {
                $related->archiveAsRelated($this);
            }

            if ($this->shouldForceDeleteRelated($name, $related) && method_exists($related, 'forceDelete')) {
                $related->forceDelete();
            } else {
                $related->delete();
            }
        }
    }

    /**
     * The instance is to be archived as related model/instance of the given parent model.
     * 
     * @param \Illuminate\Database\Eloquent\Model $parent
     * @return void
     */
    public function archiveAsRelated($parent)
    {
        $this->archivingAsRelated = true;
    }

    /**
     * Prepare the archive to be saved later once the deletion is successful.
     * 
     * @param string $name (optional)
     * @return \Dottystyle\LaravelArchiver\Eloquent\EloquentArchive
     */
    protected function prepareArchive($name = null)
    {
        return $this->getArchiveManager()->archive($this, $name ?? $this->getDefaultArchiveName());
    }

    /**
     * Save the given archive.
     * 
     * @param \Dottystyle\LaravelArchiver\Eloquent\EloquentArchive $archive
     * @return void
     */
    protected function saveArchive(EloquentArchive $archive)
    {
        $this->getArchiveManager()->save($archive);

        $this->fireModelEvent('archived', $archive);
    }

    /**
     * Get an array of key/value pairs of related things to archive.
     * 
     * @return array
     */
    public function getRelatedArchivables()
    {
        return [];
    }

    /**
     * Get the archive manager.
     * 
     * @return \Dottystyle\LaravelArchiver\EloquentArchiveManager
     */
    protected function getArchiveManager()
    {
        return app('archive')->driver('eloquent');
    }

    /**
     * Determine whether the model should be archived when being deleted or not.
     * 
     * @return bool
     */
    protected function shouldArchiveOnDelete()
    {
        return (property_exists($this, 'archived') ? $this->archived : true) && ! $this->archivingAsRelated;
    }

    /**
     * Determine whether to force delete the related model or not.
     * 
     * @param string $name
     * @param \Illuminate\Database\Eloquent\Model $related
     * @return bool
     */
    protected function shouldForceDeleteRelated($name, $related)
    {
        return true;
    }
}