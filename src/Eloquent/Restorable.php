<?php

namespace Dottystyle\LaravelArchiver\Eloquent;

interface Restorable
{
    /**
     * Restore the attributes and any relations
     * 
     * @param array $data
     * @return
     */
    public function restore(array $data);
}